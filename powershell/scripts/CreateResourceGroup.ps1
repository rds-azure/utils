param(
    [Parameter(Mandatory = $true)]
    [String] $resource_group, 

    [Parameter(Mandatory = $true)]
    [String]$location 
)

Write-Output "Creating $resource_group in $location..."
az group create --name $resource_group --location "$location"