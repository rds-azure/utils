param(
    [Parameter(Mandatory = $false)]
    [String] $resource_group, 

    [Parameter(Mandatory = $false)]
    [String]$location = "France central", 

    [Parameter(Mandatory = $true)] 
    [String]$app, 

    [Parameter(Mandatory = $true)] 
    [String]$admin_db, 
    
    [Parameter(Mandatory = $true)] 
    [String]$password_db,

    [Parameter(Mandatory = $false)] 
    [String]$test_ip
)

if (!$PSBoundParameters.ContainsKey('resource_group')) {
    $resource_group = "rg-$($app)"
    ./CreateResourceGroup -resource_group $resource_group
}

./CreateSqlServer -resource_group $resource_group -location $location -app $app -admin_db $admin_db -password_db $password_db

$server="srv-$app"
$db="db-$app"
./CreateSqlInstance -resource_group $resource_group -server $server -db_name $db

$connection_string = "Server=tcp:$server.database.windows.net,1433;Database=$db;User ID=$admin_db;Password=$password_db;Encrypt=true;Connection Timeout=30;"
Write-Output "Connection available at :"
Write-Output "$connection_string"