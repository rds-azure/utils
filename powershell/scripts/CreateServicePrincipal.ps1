param(
    [Parameter(Mandatory = $true)]
    [String] $app, 

    [Parameter(Mandatory = $false)]
    [String] $role = "Contributor",

    [Parameter(Mandatory = $true)]
    [String]$subscription, 

    [Parameter(Mandatory = $false)]
    [String]$resource_group 
)

$name = "sp-$app"
$scope = "/subscriptions/$subscription"
if ($PSBoundParameters.ContainsKey('resource_group')) {
    $scope = "$scope/resourceGroups/$resource_group" 
}
    
az ad sp create-for-rbac --name $name --role $role --scopes $scope