param(
    [Parameter(Mandatory = $true)]
    [String] $resource_group, 

    [Parameter(Mandatory = $true)] 
    [String]$server,

    [Parameter(Mandatory = $true)] 
    [String]$db_name
)

Write-Output "Creating database $db_name in server $server..."
az sql db create --resource-group $resource_group --server $server --name $db_name --service-objective Basic #--edition GeneralPurpose --family Gen5 --capacity 2 --zone-redundant true