param(
    [Parameter(Mandatory = $true)]
    [String] $resource_group, 

    [Parameter(Mandatory = $true)]
    [String]$location, 

    [Parameter(Mandatory = $true)] 
    [String]$app, 

    [Parameter(Mandatory = $true)] 
    [String]$admin_db, 
    
    [Parameter(Mandatory = $true)] 
    [String]$password_db,

    [Parameter(Mandatory = $false)] 
    [String]$test_ip
)

$server = "srv-$($app)"
$this_public_ip = (Invoke-WebRequest -uri "http://ifconfig.me/ip").Content

Write-Output "Creating sql server $server..."
az sql server create --name $server --resource-group $resource_group --location $location --admin-user $admin_db --admin-password $password_db --enable-public-network true

az sql server firewall-rule create --resource-group $resource_group --server $server -n AllowAzureIp --start-ip-address "0.0.0.0" --end-ip-address "0.0.0.0"
az sql server firewall-rule create --resource-group $resource_group --server $server -n AllowExternalIp --start-ip-address $this_public_ip --end-ip-address $this_public_ip
if ($PSBoundParameters.ContainsKey('test_ip')) {
    az sql server firewall-rule create --resource-group $resource_group --server $server -n AllowTestIp --start-ip-address $test_ip --end-ip-address $test_ip
}