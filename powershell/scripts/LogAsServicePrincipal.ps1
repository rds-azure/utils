param(
    [Parameter(Mandatory = $true)]
    [String] $tenant,

    [Parameter(Mandatory = $true)]
    [String] $app_id,

    [Parameter(Mandatory = $true)]
    [String] $app_pass
)

az login --service-principal -u $app_id -p $app_pass --tenant $tenant
