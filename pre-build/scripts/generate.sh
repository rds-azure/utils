#! /bin/sh
while getopts t:f: flag
do
    case "${flag}" in
        t) target=${OPTARG};;
        f) file=${OPTARG};;
    esac
done

export APP_NAME='cricket'
envsubst < $file > configuration.txt
cat configuration.txt

trim_double_quotes() {
    echo $1 | sed -e "s|\"||;s|\"$||"
}

while IFS='=' read key value
do
    [[ "$key" =~ \#.* ]] && continue

    if [ ! -z "$key" ]; then
        value=$(trim_double_quotes "$value")
        find $target -type f -print0 | xargs -0 sed -i "s/{{$key}}/$value/g;s/\r//g"
    fi
done < <(grep "" configuration.txt)